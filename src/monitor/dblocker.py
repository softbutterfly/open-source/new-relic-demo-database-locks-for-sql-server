# PyODBC
# https://github.com/mkleehammer/pyodbc

import os
from pathlib import Path

import pyodbc
from dotenv import find_dotenv, load_dotenv
from faker import Faker

env_file = Path(find_dotenv())

if env_file.exists():
    load_dotenv(dotenv_path=env_file)


NEW_RELIC_INSERT_KEY = os.environ["NEW_RELIC_INSERT_KEY"]
DB_DRIVER = os.getenv("DB_DRIVER")
DB_HOST = os.getenv("DB_HOST")
DB_PORT = os.getenv("DB_PORT")
DB_NAME = os.getenv("DB_NAME")
DB_USER = os.getenv("DB_USER")
DB_PASS = os.getenv("DB_PASS")

connection = pyodbc.connect(
    f"Driver={{{DB_DRIVER}}};"
    f"SERVER={DB_HOST};"
    f"PORT={DB_PORT};"
    f"DATABASE={DB_NAME};"
    f"UID={DB_USER};"
    f"PWD={DB_PASS};"
    "TrustServerCertificate=yes;"
)

cursor = connection.cursor()

print("Creating fake data")

fake = Faker()

ndata = int(5e5)
names = [fake.first_name() for _ in range(ndata)]
bday = [fake.date() for _ in range(ndata)]
data = [str(_) for _ in zip(names, bday)]

print("Done creating fake data")


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i : i + n]


_chunks = chunks(data, 1000)

print("\nInserting data into DB")
for data_chunk in _chunks:
    sql_query = """
    INSERT INTO
    sample_table (name, birthday)
    VALUES
    %s
    """ % ",\n   ".join(
        data_chunk
    )
    cursor.execute(sql_query)
