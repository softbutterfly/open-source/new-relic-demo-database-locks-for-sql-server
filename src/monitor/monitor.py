# PyODBC
# https://github.com/mkleehammer/pyodbc

import os
from pathlib import Path

import pyodbc
from dotenv import find_dotenv, load_dotenv
from newrelic_telemetry_sdk import Event, EventClient

env_file = Path(find_dotenv())

if env_file.exists():
    load_dotenv(dotenv_path=env_file)


NEW_RELIC_INSERT_KEY = os.environ["NEW_RELIC_INSERT_KEY"]
DB_DRIVER = os.getenv("DB_DRIVER")
DB_HOST = os.getenv("DB_HOST")
DB_PORT = os.getenv("DB_PORT")
DB_NAME = os.getenv("DB_NAME")
DB_USER = os.getenv("DB_USER")
DB_PASS = os.getenv("DB_PASS")

connection = pyodbc.connect(
    f"Driver={{{DB_DRIVER}}};"
    f"SERVER={DB_HOST};"
    f"PORT={DB_PORT};"
    f"DATABASE={DB_NAME};"
    f"UID={DB_USER};"
    f"PWD={DB_PASS};"
    "TrustServerCertificate=yes;"
)

cursor = connection.cursor()
sql_query = """
SELECT
    OBJECT_NAME(p.OBJECT_ID) AS table_name, resource_type, resource_description
FROM
    sys.dm_tran_locks l
JOIN
    sys.partitions p
ON
    l.resource_associated_entity_id = p.hobt_id
"""

cursor.execute(sql_query)
rows = cursor.fetchall()
headers = [
    "table_name",
    "resource_type",
    "resource_description",
]

events = [Event("DatabaseLocksSample", dict(zip(headers, row))) for row in rows]

client = EventClient(NEW_RELIC_INSERT_KEY)
response = client.send_batch(events)
response.raise_for_status()
