--
-- Set lock level for sample_table
-- Reference: https://www.sqlshack.com/es/todo-sobre-el-bloqueo-en-sql-server/
--
USE
    sample_db
GO

-- Lock scalation at table level
ALTER TABLE
    sample_table
SET
    (LOCK_ESCALATION = TABLE)
GO

-- Lock scalation at partition level
-- ALTER TABLE
--     sample_table
-- SET
--     (LOCK_ESCALATION = AUTO)
-- GO

-- Disable Lock scalation
---ALTER TABLE
---    sample_table
---SET
---    (LOCK_ESCALATION = DISABLE)
---GO
