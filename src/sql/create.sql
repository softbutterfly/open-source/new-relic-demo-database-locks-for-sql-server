---
--- Create base structure for the database.
---
CREATE DATABASE
    sample_db
GO

USE
    sample_db
GO

CREATE TABLE
    sample_table (
        id       BIGINT        NOT NULL  IDENTITY  PRIMARY KEY,
        name     VARCHAR(MAX),
        birthday DATE
    )
GO
