---
--- Som example of getting locks info from database.
---

-- Get locks info form sys.dm_tran_locks and cross reference with
-- sys.partitions.
SELECT
    OBJECT_NAME(p.OBJECT_ID) AS table_name, resource_type, resource_description
FROM
    sys.dm_tran_locks l
JOIN
    sys.partitions p
ON
    l.resource_associated_entity_id = p.hobt_id;
GO

-- Get the info form sys.partitions
SELECT
    *
FROM
    sys.partitions
GO

-- Get the info form sys.dm_tran_locks
SELECT
    *
FROM
    sys.dm_tran_locks
GO


-- Get the subset info form sys.dm_tran_locks
SELECT
    resource_type, request_mode, resource_associated_entity_id
FROM
    sys.dm_tran_locks
GO


-- Get all the posiblse locks from sys.dm_os_performance_counters
SELECT
    *
FROM
    sys.dm_os_performance_counters
WHERE
    object_name LIKE '%Locks%'
GO
