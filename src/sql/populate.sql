---
--- Add basic info to the database.
---
USE
    sample_db
GO

INSERT INTO
    sample_table (name, birthday)
VALUES
    ('andres', '2020-10-5'),
    ('martin', '2020-09-25')
GO
