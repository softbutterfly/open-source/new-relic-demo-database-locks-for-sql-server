![Sample-Code](https://gitlab.com/softbutterfly/open-source/open-source-office/-/raw/master/banners/softbutterfly-open-source--banner--sample-code.png)

![PyPI - MIT License](https://img.shields.io/pypi/l/culqi-api-python)

# New Relic Demo - Database Locks for SQL Server

Python demo to monitor database locks in SQL Server by sending custom Events to New Relic using the `newrelic_telemetry_sdk`.

## Requirements

- pyenv
- poetry
- docker
- docker-compose-plugin
- [Microsoft ODBC](https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver16#18)

## Setting up the environment

- Clone the repo and `cd` into the root directory

    ```bash
    git clone git@gitlab.com:softbutterfly/open-source/new-relic-demo-database-locks-for-sql-server.git && cd new-relic-demo-database-locks-for-sql-server
    ```

- Install the project python version using `pyevn`

    ```bash
    pyenv install $(cat .python-version)
    ```

- Configure and install `poetry` dependencies

    ```bash
    poetry config virtualenvs.in-project true #Optional

    poetry env use $(cat .python-version)
    poetry install
    ```

- Copy the `.env.db.template` in the root directory to a new file named `.env.db`, in the same place, and change the database password with any of your liking.

- `cd` into the folder `src/monitor/` and do the same with the file `.env.template`, copying it to a new `.env` file on which you will have to fill some information.
    - `NEW_RELIC_INSERT_KEY`: obtainable from your New Relic account by going to API keys > Insights insert keys
    - `DB_DRIVER`: path to sql server msodbcsql library in your system (e.g `/opt/microsoft/msodbcsql18/lib64/libmsodbcsql-18.0.so.1.1`)


## How to use

Once all the dependencies have been installed, the docker image can be set up by using `docker compose up` to have the SQL server up and running.

In order to have the database ready for operation, we need to run the `create.sql` and `populate.sql` scripts located in `src/sql`. The docker image must be running for this step.

Replace `{DB_USER}` and `{DB_PASS}` in the following command with the values set in the `.env` file

```bash
sqlcmd -S localhost,1433 -C -U {DB_USER} -P {DB_PASS} -i src/sql/create.sql -i src/sql/populate.sql
```

The following step requires the user to have two terminals, each one with a python command as follows:

- On the first terminal
    ```
    poetry run python src/monitor/dblocker.py
    ```
- Only after the first python script reaches the `"Inserting data into DB"` part, you can run
    ```
    poetry run python src/monitor/monitor.py
    ```
    This last script sends the data to New Relic, so in order for this to report anything the database has to have lock data.

    You can explore the data reported in in the Events tab or using the query builder in New Relic account.

    ![Event Query in New Relic](resources/new_relic-event-query.png)

    ![Event Query Count in New Relic](resources/new_relic-event-query2.png)
